<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Exam;
use Storage;
use Validator;

class QuestionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','role:admin,teacher']);
        $this->validation = [
            'question' => 'required|string|max:255',
            'answer' => 'required',
            'exam_id' => 'required|integer|max:255',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['exams'] = Exam::paginate(10);
        return view('exam.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $req = $request->all();
        $exam_id = ( isset($req['exam']) ) ? preg_replace('/\D/', '', $req['exam']) : null;

        if (!$exam_id) {
            return redirect('exams')->with('error','Exam no longer exist.');    
        }

        $params['exam'] = Exam::find($exam_id);

        return view('question.create',$params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('questions/create?exam='.$request->input('exam_id'))->with('error', $validator->messages());
        }

        $allowedfileExtension = ['jpg','png','jpeg','gif'];

        $question = new Question;
        $question->question = $request->input('question');
        $question->answer = json_encode($request->input('answer'));
        $question->exam_id = $request->input('exam_id');

        $a = $request->input('a');
        $b = $request->input('b');
        $c = $request->input('c');
        $d = $request->input('d');

        if($request->hasFile('a'))
        {
            $file_a = $request->file('a');

            $filename = $file_a['image']->getClientOriginalName();
            $extension = $file_a['image']->getClientOriginalExtension();
            $check = in_array($extension,$allowedfileExtension);

            if($check)
            {
                $a['image'] = str_replace('public/', '', $request->a['image']->store('public'));
            }
        }

        if($request->hasFile('b'))
        {
            $file_b = $request->file('b');

            $filename = $file_b['image']->getClientOriginalName();
            $extension = $file_b['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $b['image'] = str_replace('public/', '', $request->b['image']->store('public'));
            }
        }

        if($request->hasFile('c'))
        {
            $file_c = $request->file('c');

            $filename = $file_c['image']->getClientOriginalName();
            $extension = $file_c['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $c['image'] = str_replace('public/', '', $request->c['image']->store('public'));
            }
        }

        if($request->hasFile('d'))
        {
            $file_d = $request->file('d');

            $filename = $file_d['image']->getClientOriginalName();
            $extension = $file_d['image']->getClientOriginalExtension();

            if(in_array($extension,$allowedfileExtension))
            {
                $d['image'] = str_replace('public/', '', $request->d['image']->store('public'));
            }
        }

        $question->a = json_encode($a);
        $question->b = json_encode($b);
        $question->c = json_encode($c);
        $question->d = json_encode($d);

        $question->save();

        return redirect('exams/'.$request->input('exam_id'))->with('success','Question succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $params['question'] = Question::find($id);

        if (!$params['question']) {
            return redirect('questions/')->with('error','Question no longer exist.');
        }

        $params['questions'] = Question::where('exam_id','=',$id)->paginate(10);

        return view('question.show',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $q = Question::find($id);
        
        if (!$q) {
            return redirect('exams')->with('error','Question no longer exist.');
        }

        $params['question'] = $q;
        $params['answer'] = json_decode($q->answer);
        $params['a'] = json_decode($q->a);
        $params['b'] = json_decode($q->b);
        $params['c'] = json_decode($q->c);
        $params['d'] = json_decode($q->d);
        $params['exam'] = Exam::find($q->exam_id);

        
        // dd($params);

        return view('question.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('questions/'.$request->input('exam_id').'/edit')->with('error', $validator->messages());
        }

        $allowedfileExtension = ['jpg','png','jpeg','gif'];

        $question = Question::find($id);

        if (!$question) {
            return redirect('exams/'.$request->input('exam_id'))->with('error', 'Question no longer exist.');
        }

        $question->question = $request->input('question');
        $question->answer = json_encode($request->input('answer'));
        $question->exam_id = $request->input('exam_id');

        $a = $request->input('a');
        $b = $request->input('b');
        $c = $request->input('c');
        $d = $request->input('d');

        $img_a = json_decode( $question->a );

        // check if input has file
        if($request->hasFile('a'))
        {
            $file_a = $request->file('a');
            $filename = $file_a['image']->getClientOriginalName();
            $extension = strtolower($file_a['image']->getClientOriginalExtension());

            // check if file is valid.
            if(in_array($extension,$allowedfileExtension))
            {
                // save file
                $a['image'] = str_replace('public/', '', $request->a['image']->store('public'));

                // delete old image if exist.
                if ( isset($img_a->image) ) {
                    if (Storage::exists('public/'.$img_a->image)) {
                        Storage::delete('public/'.$img_a->image);   
                    }
                }

            } else {
                
                $a['image'] = ( isset($img_a->image) ) ? $img_a->image:'';
            }

        } else {

            if (!$request->input('image_a')) 
            {
                // Delete if no imag is set
                if ( isset($img_a->image) ) {
                    if (Storage::exists('public/'.$img_a->image)) {
                        Storage::delete('public/'.$img_a->image);   
                        $a['image'] = '';
                    }
                }

            } else {
                
                $a['image'] = ( isset($img_a->image) ) ? $img_a->image:'';
            }
        }

        $img_b = json_decode( $question->b );

        if($request->hasFile('b'))
        {
            $file_b = $request->file('b');

            $filename = $file_b['image']->getClientOriginalName();
            $extension = strtolower($file_b['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $b['image'] = str_replace('public/', '', $request->b['image']->store('public'));

                if ( isset($img_b->image) ) {
                    if (Storage::exists('public/'.$img_b->image)) {
                        Storage::delete('public/'.$img_b->image);      
                    } 
                }

            } else {
                
                $b['image'] = ( isset($img_b->image) ) ? $img_b->image:'';
            }

        } else {

            if (!$request->input('image_b')) 
            {
                // Delete if no imag is set
                if ( isset($img_b->image) ) {   
                    if (Storage::exists('public/'.$img_b->image)) {
                        Storage::delete('public/'.$img_b->image);   
                        $b['image'] = '';
                    }
                }

            } else {
                
                $b['image'] = ( isset($img_b->image) ) ? $img_b->image:'';
            }
        }

        $img_c = json_decode( $question->c );

        if($request->hasFile('c'))
        {
            $file_c = $request->file('c');

            $filename = $file_c['image']->getClientOriginalName();
            $extension = strtolower($file_c['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $c['image'] = str_replace('public/', '', $request->c['image']->store('public'));
                
                if ( isset($img_c->image) ) {
                    if (Storage::exists('public/'.$img_c->image)) {
                        Storage::delete('public/'.$img_c->image);      
                    }
                }

            } else {
                
                $c['image'] = ( isset($img_c->image) ) ? $img_c->image:'';
            }

        } else {

            if (!$request->input('image_c')) 
            {
                // Delete if no imag is set
                if ( isset($img_c->image) ) {
                    if (Storage::exists('public/'.$img_c->image)) {
                        Storage::delete('public/'.$img_c->image);     
                        $c['image'] = '';
                    }
                }

            } else {
                
                $c['image'] = ( isset($img_c->image) ) ? $img_c->image:'';
            }
        }

        $img_d = json_decode( $question->d );

        if($request->hasFile('d'))
        {
            $file_d = $request->file('d');

            $filename = $file_d['image']->getClientOriginalName();
            $extension = strtolower($file_d['image']->getClientOriginalExtension());

            if(in_array($extension,$allowedfileExtension))
            {
                $d['image'] = str_replace('public/', '', $request->d['image']->store('public'));

                if ( isset($img_d->image) ) {
                    if (Storage::exists('public/'.$img_d->image)) {
                        Storage::delete('public/'.$img_d->image);   
                    }
                }

            } else {
                
                $d['image'] = ( isset($img_d->image) ) ? $img_d->image:'';
            }

        }  else {

            if (!$request->input('image_d')) 
            {
                // Delete if no imag is set
                if ( isset($img_d->image) ) {
                    if (Storage::exists('public/'.$img_d->image)) {
                        Storage::delete('public/'.$img_d->image);   
                        $d['image'] = '';
                    }
                }

            } else {
                
                $d['image'] = ( isset($img_d->image) ) ? $img_d->image:'';
            }
        }

        $question->a = json_encode($a);
        $question->b = json_encode($b);
        $question->c = json_encode($c);
        $question->d = json_encode($d);

        $question->save();

        return redirect('questions/'.$id.'/edit')->with('success','Question succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $q = Question::find($id);
        
        if (!$q) {

            return redirect('exams')->with('error','Question no longer exists!');
        }

        $id = $q->exam_id;

        $q->delete();

        return redirect('exams/'.$id)->with('success','Question successfully removed.');
    }
}
