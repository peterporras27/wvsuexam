<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Exam;
use Validator;
use Storage;
use Auth;

class ExamsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','role:teacher,admin']);
        $this->validation = [
            'name' => 'required|string|max:255',
            'time_limit' => 'required|integer|max:255',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['exams'] = Exam::paginate(10);
        return view('exam.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exam.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('exams/create')->with('error', $validator->messages());
        }

        $exam = new Exam;
        $exam->fill( $request->all() );
        $exam->user_id = Auth::user()->id;
        $exam->save();

        return redirect('exams')->with('success','Exam succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $params['exam'] = Exam::find($id);

        if (!$params['exam']) {
            return redirect('exams')->with('error','Exam no longer exist.');
        }

        $params['questions'] = Question::where('exam_id','=',$id)->paginate(10);

        return view('exam.show',$params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $params['exam'] = Exam::find($id);

        if (!$params['exam']) {
            return redirect('exams')->with('error','Exam no longer exist.');
        }

        return view('exam.edit',$params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $exam = Exam::find($id);

        if (!$exam) {
            return redirect('exams')->with('error','Exam no longer exist.');
        }
        
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('exams/'.$id.'/edit')->with('error', $validator->messages());
        }

        $exam->fill( $request->all() );
        $exam->user_id = Auth::user()->id;
        $exam->save();

        return redirect('exams/'.$id.'/edit')->with('success','Exam succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::find($id);

        $questions = Question::where('exam_id','=',$exam->id)->get();

        if ( $questions->count() ) 
        {
            foreach ($questions as $question) 
            {
                $img_a = json_decode( $question->a );
                $img_b = json_decode( $question->b );
                $img_c = json_decode( $question->c );
                $img_d = json_decode( $question->d );

                if ( isset($img_a->image) && Storage::exists('public/'.$img_a->image) ) { 
                    Storage::delete('public/'.$img_a->image);    
                }

                if ( isset($img_b->image) && Storage::exists('public/'.$img_b->image) ) { 
                    Storage::delete('public/'.$img_b->image);    
                }

                if ( isset($img_c->image) && Storage::exists('public/'.$img_c->image) ) { 
                    Storage::delete('public/'.$img_c->image);    
                }

                if ( isset($img_d->image) && Storage::exists('public/'.$img_d->image) ) { 
                    Storage::delete('public/'.$img_d->image);    
                }

                $question->delete();
            }
        }

        if (!$exam) {
            return redirect('exams')->with('error','Exam no longer exist.');
        }

        $exam->delete();

        return redirect('exams')->with('success','Exam successfully removed.');
    }
}
