<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Result;
use App\Exam;
use Auth;
use Image;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $params['exams'] = Exam::paginate(10);

        if( Auth::user()->hasRoles(['admin','teacher']) ){
            return redirect()->route('exams.index');
        }

        $hasExam = Result::where([
            ['status', '=', 'in-progress'],
            ['user_id', '=', Auth::user()->id]
        ])->first();

        if ( $hasExam ) {
            return redirect()->route('exam',$hasExam->exam_id);
        }

        return view('admin.index', $params);
    }

    public function exam($id)
    {
        $exam =  Exam::find($id);

        // echeck if exam is completed.
        $result = Result::where([
            ['exam_id', '=', $id],
            ['user_id', '=', Auth::user()->id]
        ])->first();

        // Register exam for user
        if ( $result ) 
        {
            if ($result->status != 'in-progress') 
            {
                return redirect('home')->with('error', 'Exam already taken.');    
            }

        } else {

            $result = new Result;
            $result->exam_id = $id;
            $result->user_id = Auth::user()->id;
            $result->status = 'in-progress';
            $result->save();
        }

        $questions = Question::where('exam_id','=',$id)->inRandomOrder()->get();

        if ( $questions->count() ) 
        {
            foreach ( $questions as $question ) 
            {
                $this->create_answer([
                    'answer' => '',
                    'question_id' => $question->id,
                    'exam_id' => $id
                ]);
            }
        }

        $endtime = Carbon::parse($result->created_at)->addMinutes($exam->time_limit);
        $date = new Carbon();
        $date->setTimezone('Asia/Manila');

        if ($date->now() > $endtime) 
        {
            $correct = Answer::where([
                ['user_id','=', Auth::user()->id],
                ['exam_id','=', $exam->id],
                ['status','=', 'correct']
            ])->count();

            $wrong = Answer::where([
                ['user_id','=', Auth::user()->id],
                ['exam_id','=', $exam->id],
                ['status','=', 'wrong']
            ])->count();

            // check total 
            $result->correct_count = $correct;
            $result->wrong_count = $wrong;
            $result->status = 'completed';
            $result->save();

            return redirect('home')->with('success', 'Exam has already ended.');
        }

        $params['exam'] = $exam;
        $params['end_time'] = $endtime;
        $params['questions'] = $questions;

        return view('admin.question', $params);
    }

    public function submit_answers(Request $request)
    {
        $exam_id = $request->input('exam_id');

        $exam = Result::where([
            ['exam_id', '=', $exam_id],
            ['user_id', '=', Auth::user()->id]
        ])->first();

        if ( $exam ) {
            
            $correct = Answer::where([
                ['user_id','=', Auth::user()->id],
                ['exam_id','=', $exam_id],
                ['status','=', 'correct']
            ])->count();

            $wrong = Answer::where([
                ['user_id','=', Auth::user()->id],
                ['exam_id','=', $exam_id],
                ['status','=', 'wrong']
            ])->count();

            // check total 
            $exam->correct_count = $correct;
            $exam->wrong_count = $wrong;
            $exam->status = 'completed';
            $exam->save();
        }

        return redirect('home')->with('success', 'Exam results successfully submitted.');
    }

    public function save_answer(Request $request)
    {
        $args = [
            'answer' => $request->input('answer'),
            'question_id' => $request->input('question_id'),
            'exam_id' => $request->input('exam_id')
        ];

        $return = array(
            'error' => false,
            'data' => '',
        );

        if ( !in_array($request->input('answer'), ['a','b','c','d']) ) 
        {
            $return['error'] = true;
            return $return;
        }

        $this->create_answer( $args );

        return $return;
    }

    public function create_answer($args)
    {
        $answer = $args['answer'];
        $question_id = $args['question_id'];
        $exam_id = $args['exam_id'];

        $ans = Answer::where([
            ['question_id','=', $question_id],
            ['user_id','=', Auth::user()->id],
            ['exam_id','=', $exam_id]
        ])->first();

        if ( !$ans ) { $ans = new Answer; }

        $ans->answer = $answer;
        $ans->question_id = $question_id;
        $ans->exam_id = $exam_id;
        $ans->user_id = Auth::user()->id;

        // check if answer is correct
        $question = Question::find($question_id);

        $correct = json_decode( $question->answer );

        if ( in_array($answer, $correct) ) 
        {
            $ans->status = 'correct';

        } else {

            $ans->status = 'wrong';
        }

        $ans->save();
    }

    public function img($filename)
    {
        return Image::make(Storage::get('public/'.$filename))->response();
    }
}
