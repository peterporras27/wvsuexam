<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Result;
use Auth;

class Exam extends Model
{
    protected $fillable = [
        'name', 'time_limit', 'user_id', 'instructions'
    ];

    public function isCompleted()
    {
    	return Result::where([
    		['user_id','=', Auth::user()->id],
    		['exam_id','=', $this->id],
    		['status','=', 'completed'],
    	])->count();
    }

    public function result()
    {
    	return Result::where([
    		['user_id','=', Auth::user()->id],
    		['exam_id','=', $this->id]
    	])->first();
    }
}
