<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{

    public function student()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function exam()
    {
        return $this->hasOne(Exam::class,'id','exam_id');
    }
}
