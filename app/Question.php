<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Answer;
use Auth;

class Question extends Model
{
    protected $fillable = [
        'question', 'answer', 'exam_id'
    ];

    public function getAnswer()
    {
    	$ans = Answer::where([
    		['question_id','=', $this->id],
    		['user_id','=', Auth::user()->id]
    	])->first();

    	$return = '';

    	if ($ans) {
    		$return = $ans->answer;
    	}

    	return $return;
    }
}
