<?php

namespace App\Exports;

use App\Result;
use App\Exam;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ResultExport implements FromView
{
    protected $examid;

    public function __construct($examid)
    {
        $this->examid = $examid;
    }

    public function view(): View
    {
        $results = Result::where('exam_id','=',$this->examid)->get();
        return view('results.print', ['results' => $results]);
    }
}
