<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[
        		'username' => 'admin',
	            'email' => 'admin@wvsuexam.com',
                'first_name' => '',
                'last_name' => '',
                'middle_name' => '',
	            'role' => 'admin',
                'pwd' => '',
	            'password' => Hash::make('password')
        	],[
                'username' => 'student',
                'email' => 'student@wvsuexam.com',
                'first_name' => 'Ray Adriane',
                'last_name' => 'Gaje',
                'middle_name' => 'Gaje',
                'role' => 'student',
                'pwd' => 'password',
                'password' => Hash::make('password')
            ]
        ]);
    }
}
