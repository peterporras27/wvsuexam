@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">{{ $exam->name }} Questions</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">Exams</a></li>
            <li class="breadcrumb-item active">Exam Details</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			Questions <a href="{{ route('questions.create',['exam'=>$exam->id]) }}" class="btn btn-info btn-sm float-right">Create question <i class="fa fa-plus"></i></a>
		</h5>
		<div class="card-body">
			
			@if($questions->count())

			<table class="table table-hover table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Question</th>
						<th scope="col" class="text-right">Options</th>
					</tr>
				</thead>
				<tbody>
					@foreach($questions as $question)
					<tr>
						<td>{{ $question->question }}</td>
						<td>
							<div class="float-right">
							<a href="{{ route('questions.show',$question->id) }}" class="btn btn-sm btn-primary">
								View Selections <i class="fa fa-eye"></i>
							</a>
							| 
							<a href="{{ route('questions.destroy',$question->id) }}" class="btn btn-sm btn-danger">
								Delete <i class="fa fa-times"></i>
							</a>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $questions->links() }}

			@else
			
			<div class="alert alert-warning">
				<strong>Oops!</strong> There are no records to show at the moment.
			</div>

			@endif

		</div>
	</div>
</div>
@endsection