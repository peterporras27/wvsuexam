@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">{{ $exam->name }}</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">Exams</a></li>
            <li class="breadcrumb-item"><a href="{{ route('exams.show',$exam->id) }}">{{ $exam->name }}</a></li>
            <li class="breadcrumb-item active">Question</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			Edit Question
		</h5>
		<div class="card-body">
			
			<form action="{{ route('questions.update',$question->id) }}" method="POST" role="form" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="_method" value="PUT">
				<input type="hidden" name="exam_id" value="{{ $exam->id }}">
				<div class="row">
					<div class="col">
						<div class="form-group">
							<h5>Question</h5>
							<textarea class="form-control" rows="5" placeholder="What is your question?" id="question-name" name="question">{{ $question->question }}</textarea>
							@error('question')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					
					</div>
				</div>
				
				<div class="row">
					<div class="col">
						<h5>Selections</h5>
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col">

						<div class="card bg-light mb-3">
							<div class="card-header">A</div>
						  	<div class="card-body">
				  				<div class="form-group">
									<input type="checkbox" name="answer[]" value="a" class="form-control js-switch" {{ in_array('a', $answer) ? 'checked':'' }}> 
									<span class="avail badge badge-{{ in_array('a', $answer) ? 'success':'warning' }}">{{ in_array('a', $answer) ? 'Correct Answer':'Wrong Answer' }}</span>
								</div> 
								<div class="form-group">
									<input type="text" name="a[text]" class="form-control" value="{{ $a->text }}" placeholder="Answer">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Upload</span>
									</div>
									<div class="custom-file">
										<input type="file" name="a[image]" class="custom-file-input" data-id="a" id="imgupload-a">
										<label class="custom-file-label" for="imgupload-a">Choose image</label>
									</div>
								</div>
								<div class="preview-a">
									@if(isset($a->image) && !empty($a->image))
									<img src="{{ route('img',$a->image) }}" alt="" class="img-fluid img-thumbnail">
									<input type="hidden" name="image_a" value="1">
									<a href="#" onclick="remImg('a')" class="closeme badge badge-pill badge-danger"><i class="fa fa-times"></i></a>
									@endif
								</div>
						  	</div>
						</div>
						
						<div class="card bg-light mb-3">
							<div class="card-header">B</div>
						  	<div class="card-body">
						  		<div class="form-group">
									<input type="checkbox" name="answer[]" value="b" class="form-control js-switch" {{ in_array('b', $answer) ? 'checked':'' }}> 
									<span class="avail badge badge-{{ in_array('b', $answer) ? 'success':'warning' }}">{{ in_array('b', $answer) ? 'Correct Answer':'Wrong Answer' }}</span>
								</div> 
								<div class="form-group">
									<input type="text" name="b[text]" class="form-control" value="{{ $b->text }}" placeholder="Answer">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Upload</span>
									</div>
									<div class="custom-file">
										<input type="file" name="b[image]" class="custom-file-input" data-id="b" id="imgupload-b">
										<label class="custom-file-label" for="imgupload-b">Choose image</label>
									</div>
								</div>
								<div class="preview-b">
									@if(isset($b->image) && !empty($b->image))
									<img src="{{ route('img',$b->image) }}" alt="" class="img-fluid img-thumbnail">
									<input type="hidden" name="image_b" value="1">
									<a href="#" onclick="remImg('b')" class="closeme badge badge-pill badge-danger"><i class="fa fa-times"></i></a>
									@endif
								</div>
						  	</div>
						</div>

						
					</div>
					<div class="col">

						<div class="card bg-light mb-3">
							<div class="card-header">C</div>
						  	<div class="card-body">
						  		<div class="form-group">
									<input type="checkbox" name="answer[]" value="c" class="form-control js-switch" {{ in_array('c', $answer) ? 'checked':'' }}> 
									<span class="avail badge badge-{{ in_array('c', $answer) ? 'success':'warning' }}">{{ in_array('c', $answer) ? 'Correct Answer':'Wrong Answer' }}</span>
								</div> 
								<div class="form-group">
									<input type="text" name="c[text]" class="form-control" value="{{ $c->text }}" placeholder="Answer">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Upload</span>
									</div>
									<div class="custom-file">
										<input type="file" name="c[image]" class="custom-file-input" data-id="c" id="imgupload-c">
										<label class="custom-file-label" for="imgupload-c">Choose image</label>
									</div>
								</div>
								<div class="preview-c">
									@if(isset($c->image) && !empty($c->image))
									<img src="{{ route('img',$c->image) }}" alt="" class="img-fluid img-thumbnail">
									<input type="hidden" name="image_c" value="1">
									<a href="#" onclick="remImg('c')" class="closeme badge badge-pill badge-danger"><i class="fa fa-times"></i></a>
									@endif
								</div>
						  	</div>
						</div>

						<div class="card bg-light mb-3">
							<div class="card-header">D</div>
						  	<div class="card-body">
						  		<div class="form-group">
									<input type="checkbox" name="answer[]" value="d" class="form-control js-switch" {{ in_array('d', $answer) ? 'checked':'' }}> 
									<span class="avail badge badge-{{ in_array('d', $answer) ? 'success':'warning' }}">{{ in_array('d', $answer) ? 'Correct Answer':'Wrong Answer' }}</span>
								</div> 
								<div class="form-group">
									<input type="text" name="d[text]" class="form-control" value="{{ $d->text }}" placeholder="Answer">
								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text">Upload</span>
									</div>
									<div class="custom-file">
										<input type="file" name="d[image]" class="custom-file-input" data-id="d" id="imgupload-d">
										<label class="custom-file-label" for="imgupload-d">Choose image</label>
									</div>
								</div>
								<div class="preview-d">
									@if(isset($d->image) && !empty($d->image))
									<img src="{{ route('img',$d->image) }}" class="img-fluid img-thumbnail">
									<input type="hidden" name="image_d" value="1">
									<a href="#" onclick="remImg('d')" class="closeme badge badge-pill badge-danger"><i class="fa fa-times"></i></a>
									@endif
								</div>
						  	</div>
						</div>

						
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Update &nbsp;<i class="fa fa-save"></i></button>
			</form>

		</div>
	</div>
</div>
@endsection
@section('header')
<style>
.closeme{
	position: absolute;
    margin-top: -9px;
    padding: 5px 10px;
    margin-left: -15px;
}
</style>
@endsection
@section('footer')
<script>
var $=jQuery;
function remImg(id)
{
	$('.preview-'+id).html('');
	$('#imgupload-'+id).val('');
}
// Multiple images preview in browser
var imagesPreview = function(input, placeToInsertImagePreview) 
{
    if (input.files) 
    {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) 
        {
            var reader = new FileReader();
            reader.onload = function(event) {
            	var r = '<img class="img-fluid img-thumbnail" src="'+event.target.result+'">';
                $($.parseHTML(r)).appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};

$('.custom-file-input').on('change', function() {
	var i = $(this).data('id');
	$('.preview-'+i).html('');
    imagesPreview(this, 'div.preview-'+i);
});

$(document).ready(function(){

	// $('#question-name').summernote({
	// 	height: 200,                 // set editor height
	// 	minHeight: null,             // set minimum height of editor
	// 	maxHeight: null,             // set maximum height of editor
	// });	

	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	elems.forEach(function(html) {
	  var switchery = new Switchery(html);
	});

	$('.js-switch').change(function(){

		if ($(this).prop('checked')) {

			$(this).parent().find('.avail').text('Correct Answer');
			$(this).parent().find('.avail').removeClass('badge-warning');
			$(this).parent().find('.avail').addClass('badge-success');

		} else {

			$(this).parent().find('.avail').text('Wrong Answer');
			$(this).parent().find('.avail').removeClass('badge-success');
			$(this).parent().find('.avail').addClass('badge-warning');
		}
	});
});


</script>
@endsection