@extends('layouts.public')
@section('content')
<div id="layoutAuthentication">
    <div id="layoutAuthentication_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div align="center">
                            <h2>SICT Aptitude Test </h2>
                        </div>
                        <div class="card shadow-lg border-0 rounded-lg mt-5">
                            <div class="card-header" align="center">
                                {{-- <h5 class="text-center font-weight-light my-4">Login</h5> --}}
                                <b>Login</b>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label class="small mb-1" for="username">Username</label>
                                        <input class="form-control py-4 @error('username') is-invalid @enderror" id="username" type="text" placeholder="Enter username" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus />
                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="small mb-1" for="password">Password</label>
                                        <input class="form-control py-4 @error('password') is-invalid @enderror" id="password" type="password" placeholder="Enter password" name="password" required autocomplete="current-password" />
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    {{-- 
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="rememberPasswordCheck" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                                            <label class="custom-control-label" for="rememberPasswordCheck">Remember password</label>
                                        </div>
                                    </div> 
                                    --}}
                                    <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                        @if (Route::has('password.request'))
                                            <a class="small" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                        <button type="submit" class="btn btn-primary btn-lg">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                            @if (Route::has('register'))
                            <div class="card-footer text-center">
                                <div class="small"><a href="{{ route('register') }}">Need an account? Sign up!</a></div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection