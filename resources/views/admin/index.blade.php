@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Available Exams</h1>
        <hr>
    </div>

    @if( Auth::user()->isStudent() )
    <div class="container-fluid">
    	<div class="row">
    		
			@if( $exams->count() )

    			@foreach($exams as $exam )
    			<div class="col-md-3">
	    			<div class="card{{ ($exam->isCompleted()) ? ' bg-info text-white':' bg-light' }}">
	    				<div class="card-body">
	    					<h4 class="card-title">{{ $exam->name }}</h4>
	    					<p class="card-text">Duration: {{ $exam->time_limit }} minutes</p>
                            @if( $exam->isCompleted() )
                            <div class="alert alert-info mb-0" role="alert">
                                <h5 class="alert-heading mb-0">Completed! </h5>
                                <hr>
                                <h5 class="alert-heading mb-0">Score: {{ $exam->result()->correct_count.' / '. ($exam->result()->correct_count+$exam->result()->wrong_count) }}</h5>
                            </div>
                            @else
                                <a href="{{ route( 'exam', $exam->id ) }}" class="btn btn-info">
                                    Begin Test &nbsp;<i class="fa fa-edit"></i>
                                </a>
                            @endif
	    				</div>
	    			</div>
    			</div>

    			@endforeach

    			{{ $exams->links() }}

			@else
		
			<div class="alert alert-warning">
				There are no available exams to show at the moment.
			</div>

			@endif

		
    	</div>
    </div>
    @endif
</main>
@endsection