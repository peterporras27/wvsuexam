@extends('home')
@section('header')
<style>
.questions input{
    width: 50px;
    display: inline-block;
}
.choices p{
    margin-top: 5px;
    margin-left:15px;
}

.choices .form-control:focus{
    box-shadow: none;
}
.steps{display: none;}
ul{list-style: none;}
ul li{display: inline-block;}
.actions{margin: 60px 0;}
.actions ul{padding-left: 0;}
.actions ul li a{
    margin-right: 15px;
    text-decoration: none;
    padding: 15px 30px;
    color: #fff;
    background-color: #17a2b8;
    border-color: #17a2b8;
    border-radius: 25px;
    width: 160px;
    display: inline-block;
    text-align: center;
}
.actions ul li a:hover{
    text-decoration: none;
    background-color: #138496;
    border-color: #117a8b;
}
</style>
@endsection
@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h1 class="mt-4">{{ $exam->name }}</h1>
            </div>
            <div class="col-md-6">
                <h1 class="mt-4">
                    <div id="timer" class="float-right">
                        <span id="hours"></span>
                        <span id="minutes"></span>
                        <span id="seconds"></span>
                    </div>
                </h1>
            </div>
        </div>
        <hr>
    </div>
    <div class="container-fluid">
    	
        

		@if( $questions->count() )

            <div class="row">

                <div id="steps-basic" class="col">

                @if( !empty($exam->instructions) )
                    <h3></h3>
                    <section><h4>{{ $exam->instructions }}</h4></section>
                @endif

    			@foreach($questions as $question )
                    <h3></h3>
                    <section>
        			<div class="card">
        				<div class="card-body questions">
                            <h3>{{ $question->question }}</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media choices">
                                                <input type="radio" {{ $question->getAnswer() == 'a' ? 'checked':'' }} data-exam="{{ $exam->id }}" data-id="{{ $question->id }}" name="answer" value="a" class="form-control"> 
                                                <div class="media-body">
                                                   
                                                    <p>{{ json_decode( $question->a )->text }}</p>
                                                    
                                                    @if(isset(json_decode( $question->a )->image) && !empty(json_decode( $question->a )->image))
                                                        <img src="{{ route('img',json_decode( $question->a )->image) }}" alt="" class="img-fluid img-thumbnail">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <hr>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media choices">
                                                <input type="radio" {{ $question->getAnswer() == 'b' ? 'checked':'' }} data-exam="{{ $exam->id }}" data-id="{{ $question->id }}" name="answer" value="b" class="form-control"> 
                                                <div class="media-body">
                                                   
                                                        <p>{{ json_decode( $question->b )->text }}</p>
                                                    
                                                    @if(isset(json_decode( $question->b )->image) && !empty(json_decode( $question->b )->image))
                                                        <img src="{{ route('img',json_decode( $question->b )->image) }}" alt="" class="img-fluid img-thumbnail">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media choices">
                                                <input type="radio" {{ $question->getAnswer() == 'c' ? 'checked':'' }} data-exam="{{ $exam->id }}" data-id="{{ $question->id }}" name="answer" value="c" class="form-control"> 
                                                <div class="media-body">
                                                   
                                                        <p>{{ json_decode( $question->c )->text }}</p>
                                                    
                                                    @if(isset(json_decode( $question->c )->image) && !empty(json_decode( $question->c )->image))
                                                        <img src="{{ route('img',json_decode( $question->c )->image) }}" alt="" class="img-fluid img-thumbnail">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="media choices">
                                                <input type="radio" {{ $question->getAnswer() == 'd' ? 'checked':'' }} data-exam="{{ $exam->id }}" data-id="{{ $question->id }}" name="answer" value="d" class="form-control"> 
                                                <div class="media-body">
                                                   
                                                        <p>{{ json_decode( $question->d )->text }}</p>
                                                    
                                                    @if(isset(json_decode( $question->d )->image) && !empty(json_decode( $question->d )->image))
                                                        <img src="{{ route('img',json_decode( $question->d )->image) }}" alt="" class="img-fluid img-thumbnail">
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
        				</div>
        			</div>
                    </section>
    			@endforeach

                </div>

                <form id="final" action="{{ route('final') }}" method="POST">
                    @csrf
                    <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                </form>
            </div>

		@else

    	    <div class="row">
        		<div class="alert alert-warning">
        			There are no available questions to show at the moment.
        		</div>
            </div>

		@endif
	
    </div>
</main>
@endsection
@section('footer')
<script src="{{ asset('js/jquery.steps.min.js') }}"></script>
<script>
function makeTimer() {

    var endTime = new Date("{{$end_time}}");
    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var hours = '00';
    var minutes = '00';
    var seconds = '00';

    if ( timeLeft > 0 ) 
    {
        var days = Math.floor(timeLeft / 86400); 
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

    } else {

        jQuery('#submitform').submit();
    }

    jQuery("#hours").html(hours + "<span>:</span>");
    jQuery("#minutes").html(minutes + "<span>:</span>");
    jQuery("#seconds").html(seconds + "<span></span>");
}

setInterval(function() { makeTimer(); }, 1000);

jQuery(document).ready(function($) {

    $("#steps-basic").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        autoFocus: true,
        onFinishing: function (event, currentIndex) { 
            $('#final').submit();
            return true; 
        }
    });

    $('[name="answer"]').change(function(event) {

        var answer = $(this).val();
        var question_id = $(this).data('id');
        var exam_id = $(this).data('exam');
        
        var data = {
            answer: answer,
            question_id: question_id,
            exam_id: exam_id,
            _token: '{{ csrf_token() }}'
        };

        $.ajax({
            url: '{{ route('answer') }}',
            type: 'POST',
            dataType: 'json',
            data: data,
        }).always(function(response) {

            console.log(response);
        });
        
    });
});
</script>
@endsection