<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>WVSU JC - Exam</title>
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
        <script src="{{asset('fontawesome/js/all.min.js')}}" crossorigin="anonymous"></script>
    </head>
    <body class="" style="padding-top: 60px; background: url('{{ asset('img/logo.png') }}') no-repeat center center;background-size: 50%;">
        
        @yield('content')
        
        <script src="{{asset('js/jquery-3.4.1.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('js/bootstrap.bundle.min.js')}}" crossorigin="anonymous"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
    </body>
</html>
