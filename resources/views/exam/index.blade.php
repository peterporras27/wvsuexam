@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Exam</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active">Exam lists</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			Lists <a href="{{ route('exams.create') }}" class="btn btn-info btn-sm float-right">Create Exam <i class="fa fa-plus"></i></a>
		</h5>
		<div class="card-body">
			
			@if($exams->count())

			<table class="table table-hover table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Duration</th>
						<th scope="col" class="text-right">Options</th>
					</tr>
				</thead>
				<tbody>
					@foreach($exams as $exam)
					<tr>
						<td>{{ $exam->name }}</td>
						<td>{{ $exam->time_limit }} minutes</td>
						<td>
							<div class="float-right">
								<a href="{{ route('exams.edit',$exam->id) }}" class="btn btn-sm btn-success">
									Edit <i class="fa fa-edit"></i>
								</a>
								
								<a href="{{ route('results.index',['exam'=>$exam->id]) }}" class="btn btn-sm btn-info">
									View Results <i class="fa fa-eye"></i>
								</a>
								
								<a href="{{ route('exams.show',$exam->id) }}" class="btn btn-sm btn-primary">
									View Questions <i class="fa fa-eye"></i>
								</a>

								<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal-{{$exam->id}}">
									Delete <i class="fa fa-times"></i>
								</button>

								<div class="modal fade" id="deleteModal-{{$exam->id}}" tabindex="-1" aria-labelledby="deleteModal-{{$exam->id}}Label" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="{{ route('exams.destroy',$exam->id) }}" method="POST" role="form">
												@csrf
												@method('DELETE')
												<div class="modal-header">
													<h5 class="modal-title" id="deleteModal-{{$exam->id}}Label">Delete</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<b>{{ $exam->name }}</b><br> Are you sure?

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
													<button type="submit" class="btn btn-danger">Delete</button>
												</div>
											</form>
										</div>
									</div>
								</div>

							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $exams->links() }}

			@else
			
			<div class="alert alert-warning">
				<strong>Oops!</strong> There are no records to show at the moment.
			</div>

			@endif



		</div>
	</div>
</div>
@endsection