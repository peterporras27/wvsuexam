@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Edit Exams</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">Exams</a></li>
            <li class="breadcrumb-item active">Edit Exam</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			Exam details
		</h5>
		<div class="card-body">
			
			<form action="{{ route('exams.update',$exam->id) }}" method="POST" role="form">
				@csrf
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col">
						<div class="form-group">
							<label for="exam-name">Name:</label>
							<input type="text" class="form-control" required id="exam-name" name="name" value="{{ $exam->name }}">
							@error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>
					<div class="col">
						<div class="form-group">
							<label for="exam-duration">Exam Duration (minutes):</label>
							<input type="number" value="{{ $exam->time_limit }}" class="form-control" id="exam-duration" required name="time_limit" placeholder="60">
							@error('time_limit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="exam-instructions">Exam Instructions:</label>
					<textarea rows="5" class="form-control" required id="exam-instructions" name="instructions">{{ $exam->instructions }}</textarea>
					@error('instructions')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				</div>
				<button type="submit" class="btn btn-primary">Save &nbsp;<i class="fa fa-save"></i></button>
			</form>

		</div>
	</div>
</div>
@endsection