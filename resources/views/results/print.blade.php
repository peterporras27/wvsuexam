<table>
    <thead>
    <tr>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Middle Name</th>
		<th>Username</th>
		<th>Email</th>
		<th>Correct Answers</th>
		<th>Wrong Answers</th>
    </tr>
    </thead>
    <tbody>
    @foreach($results as $result)
		<tr>
			<td>{{ isset($result->student->first_name) ? $result->student->first_name:'' }}</td>
			<td>{{ isset($result->student->last_name) ? $result->student->last_name:'' }}</td>
			<td>{{ isset($result->student->middle_name) ? $result->student->middle_name:'' }}</td>
			<td>{{ isset($result->student->username) ? $result->student->username:'' }}</td>
			<td>{{ isset($result->student->email) ? $result->student->email:'' }}</td>
			<td>{{ $result->correct_count }}</td>
			<td>{{ $result->wrong_count }}</td>
		</tr>
	@endforeach
    </tbody>
</table>