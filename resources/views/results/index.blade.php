@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">
        	{{ $exam->name }} Results
        	<a href="{{ route('results.index',['exam'=>$exam->id,'print'=>1]) }}" target="_blank" class="float-right btn btn-info">
        		Print Results
        	</a>
        </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('exams.index') }}">Exams</a></li>
            <li class="breadcrumb-item active">{{ $exam->name }}</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">Lists</h5>
		<div class="card-body">
			
			@if( $results->count() )

			<table class="table table-hover table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Exam Title</th>
						<th scope="col">Time Limit</th>
						<th scope="col">Correct Answers</th>
						<th scope="col">Wrong Answers</th>
						<th scope="col">Status</th>
						<th scope="col">Time Started</th>
						<th scope="col">Time Finished</th>
					</tr>
				</thead>
				<tbody>
					@foreach($results as $result)
					<tr>
						<td>
							{{ isset($result->student->first_name) ?  $result->student->first_name:'' }}
							{{ isset($result->student->last_name) ? $result->student->last_name:'' }}
						</td>
						<td>{{ isset($result->exam->name) ? $result->exam->name:'' }}</td>
						<td>{{ isset($result->exam->time_limit) ? $result->exam->time_limit:'' }}</td>
						<td>{{ $result->correct_count }}</td>
						<td>{{ $result->wrong_count }}</td>
						<td>{{ ucwords($result->status) }}</td>
						<td>{{ \Illuminate\Support\Carbon::parse($result->created_at)->format('F j, Y g:i a') }}</td>
						<td>{{ \Illuminate\Support\Carbon::parse($result->updated_at)->format('F j, Y g:i a') }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $results->links() }}

			@else
			
			<div class="alert alert-warning">
				<strong>Oops!</strong> There are no records to show at the moment.
			</div>

			@endif



		</div>
	</div>
</div>
@endsection