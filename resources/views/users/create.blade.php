@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Register User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
            <li class="breadcrumb-item active">Register User</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			User details
		</h5>
		<div class="card-body">
			
			<form action="{{ route('users.store') }}" method="POST" role="form">
				@csrf
				<div class="row">
					<div class="col">

						<div class="form-group">
							<label for="user-name">Username</label>
							<input type="text" class="form-control" required id="user-name" name="username" value="{{ old('username') }}">
							@error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" required name="email" placeholder="" value="{{ old('email') }}">
							@error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="role">User Role</label>
							<select class="form-control" id="role" required name="role">
								<option value="student"{{ old('role')=='student' ? ' selected':'' }}>Student</option>
								<option value="admin"{{ old('role')=='admin' ? ' selected':'' }}>Admin</option>
							</select>
							@error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						
					</div>
					<div class="col">

					
						<div class="form-group">
							<label for="first-name">First Name</label>
							<input type="text" class="form-control" id="first-name" required name="first_name" placeholder="" value="{{ old('first_name') }}">
							@error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="last-name">Last Name</label>
							<input type="text" class="form-control" id="last-name" required name="last_name" placeholder="" value="{{ old('last_name') }}">
							@error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="middle-name">Middle Name</label>
							<input type="text" class="form-control" id="middle-name" required name="middle_name" placeholder="" value="{{ old('middle_name') }}">
							@error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

					</div>
					<div class="col">
						<div class="form-group">
							<label for="email">Password</label>
							<input type="password" class="form-control" id="password" required name="password" placeholder="">
							@error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="password_confirmation">Confirm Password</label>
							<input type="password" class="form-control" id="password_confirmation" required name="password_confirmation" placeholder="">
							@error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

					</div>
				</div>
				<button type="submit" class="btn btn-primary float-right">Register &nbsp;<i class="fa fa-save"></i></button>
			</form>

		</div>
	</div>
</div>
@endsection