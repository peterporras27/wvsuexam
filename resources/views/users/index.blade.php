@extends('home')
@section('content')
<main>
    <div class="container-fluid">

    	<div class="row">
    		<div class="col">
    			<h1 class="mt-4">Users</h1>
    		</div>
    		<div class="col"></div>
			<div class="col">
				<form action="{{ route('users.index') }}" method="GET">
					<div class="input-group mb-3 mt-4">
						<input type="text" name="s" value="{{ $search }}" placeholder="Name" class="form-control">
						<select class="form-control" name="r" placeholder="role" aria-label="role" aria-describedby="button-addon2">
							<option value="">All</option>
							<option value="student"{{ $role == 'student' ? ' selected':'' }}>Student</option>
							<option value="admin"{{ $role == 'admin' ? ' selected':'' }}>Admin</option>
						</select>
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
						</div>
					</div>
				</form>		
			</div>
		</div>

        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active">User lists</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			Lists <a href="{{ route('users.create') }}" class="btn btn-info btn-sm float-right">Register User <i class="fa fa-plus"></i></a>
		</h5>
		<div class="card-body">
			
			
			

			@if($users->count())

			<table class="table table-hover table-bordered">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Username</th>
						<th scope="col">Password</th>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">Middle Name</th>
						{{-- <th scope="col">Email</th> --}}
						<th scope="col">User Role</th>
						<th scope="col" class="text-right">Options</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{ $user->username }}</td>
						<td>
							@if($user->pwd)
								<input class="form-control" type="text" value="{{ $user->pwd }}">
							@endif
						</td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->middle_name }}</td>
						{{-- <td>{{ $user->email }}</td> --}}
						<td>{{ $user->role }}</td>
						<td>
							<div class="float-right">
							<a href="{{ route('users.edit',$user->id) }}" class="btn btn-sm btn-success">
								Edit <i class="fa fa-edit"></i>
							</a>
							| 
							<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-{{ $user->id }}">
								Delete <i class="fa fa-times"></i>
							</button>
							</div>
							<div class="modal fade" id="delete-{{ $user->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<form action="{{ route('users.destroy',$user->id) }}" method="POST" role="form">
											@csrf
											<input type="hidden" name="_method" value="DELETE">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Delete User</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<p>Are you sure?</p>
												<p>All exam records for this user will also be removed.</p>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
												<button type="submit" class="btn btn-danger">Delete</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			{{ $users->links() }}

			@else
			
			<div class="alert alert-warning">
				<strong>Oops!</strong> There are no records to show at the moment.
			</div>

			@endif



		</div>
	</div>
</div>
@endsection