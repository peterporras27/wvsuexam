@extends('home')
@section('content')
<main>
    <div class="container-fluid">
        <h1 class="mt-4">Edit User</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
            <li class="breadcrumb-item active">Edit User</li>
        </ol>
    </div>
</main>
<div class="container-fluid">
	<div class="card">
		<h5 class="card-header">
			User details
		</h5>
		<div class="card-body">
			
			<form action="{{ route('users.update',$user->id) }}" method="POST" role="form">
				@csrf
				<input type="hidden" name="_method" value="PUT">
				<div class="row">
					<div class="col">
						
						<div class="form-group">
							<label for="user-name">Username</label>
							<input type="text" class="form-control" required id="user-name" name="username" value="{{ $user->username }}">
							@error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="email" required name="email" placeholder="" value="{{ $user->email }}">
							@error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="role">User Role</label>
							<select class="form-control" id="role" required name="role">
								<option value="student"{{ $user->role=='student' ? ' selected':'' }}>Student</option>
								<option value="admin"{{ $user->role=='admin' ? ' selected':'' }}>Admin</option>
							</select>
							@error('role')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

					</div>
					<div class="col">
					
						<div class="form-group">
							<label for="first-name">First Name</label>
							<input type="text" class="form-control" id="first-name" required name="first_name" placeholder="" value="{{ $user->first_name }}">
							@error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="last-name">Last Name</label>
							<input type="text" class="form-control" id="last-name" required name="last_name" placeholder="" value="{{ $user->last_name }}">
							@error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="middle-name">Middle Name</label>
							<input type="text" class="form-control" id="middle-name" required name="middle_name" placeholder="" value="{{ $user->middle_name }}">
							@error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

					</div>
					<div class="col">
						<div class="form-group">
							<label for="email">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="">
							@error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>

						<div class="form-group">
							<label for="password_confirmation">Confirm Password</label>
							<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="">
							@error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary float-right">Save &nbsp;<i class="fa fa-save"></i></button>
			</form>

		</div>
	</div>
</div>
@endsection