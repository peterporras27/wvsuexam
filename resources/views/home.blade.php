<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - SICT Aptitude Test</title>
        <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
        <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
        <link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/switchery.min.css')}}" rel="stylesheet">
        <script src="{{asset('fontawesome/js/all.min.js')}}" crossorigin="anonymous"></script>
        @yield('header')
    </head>
    <body class="sb-nav-fixed{{ Auth::user()->hasRoles(['student']) ? ' sb-sidenav-toggled':'' }}">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ route('home') }}">SICT Aptitude Test</a>
            {{-- <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button> --}}
            <!-- Navbar-->
            <ul class="navbar-nav ml-auto mr-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{Auth::user()->username}} <i class="fas fa-user fa-fw"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        {{-- <a class="dropdown-item" href="#">Settings</a> --}}
                        {{-- <div class="dropdown-divider"></div> --}}
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
                    </div>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">

                            <div class="sb-sidenav-menu-heading">Main</div>

                            {{-- <a class="nav-link" href="{{ route('home') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a> --}}

                            @if(Auth::user()->hasRoles(['admin','teacher']))

                            {{-- <a class="nav-link" href="{{ route('results.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Results
                            </a>

                            <div class="sb-sidenav-menu-heading">Interface</div> --}}

                            <a class="nav-link" href="{{ route('exams.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Exams
                            </a>

                            @endif

                            @if(Auth::user()->isAdmin())

                            <a class="nav-link" href="{{ route('users.index') }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Users
                            </a>

                            @endif
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Logged in as:</div>
                        {{Auth::user()->username}}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">


                @yield('content')

                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; {{ date('Y') }}</div>
                            <div>
                               
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('js/toastr.min.js')}}"></script>
        <script src="{{asset('summernote/summernote.min.js')}}"></script>
        <script src="{{asset('js/switchery.min.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        @foreach ( $errors->all() as $message )
            <script>toastr.warning('{{ $message }}', 'Error!')</script>
        @endforeach
        @if(session('success'))
            <script>toastr.success('{{ session('success') }}', 'Success!')</script>
        @endif
        @if(session('error'))
            <script>toastr.error('{{ session('error') }}', 'Error!')</script>
        @endif
        @yield('footer')
    </body>
</html>
